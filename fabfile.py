import posixpath
import os
import re
import fabtools
from fabtools.files import is_dir as _is_dir
from fabtools.require import python, files, nginx, deb
from fabric.contrib.console import confirm as confirm_global
from fabric.api import *
from fabsettings import *
from import_file import import_file

SCRIPT_PATH  = os.path.dirname(__file__)
env.lcwd     = os.path.abspath(SCRIPT_PATH)
env.user     = DEPLOY_USER
env.password = DEPLOY_PASSWORD
env.hosts    = DEPLOY_HOSTS
env.no_input = True
env.debug    = True

env.deploy_tools_path  = os.path.join(SCRIPT_PATH, '..')
env.domain_default     = DEPLOY_DEFAULT_DOMAIN
env.repository_default = DEPLOY_DEFAULT_REPOSITORY

LOCAL_CONF_TEMPLATES = os.path.join(SCRIPT_PATH, 'template', 'conf')
VALIDATOR = '^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$'

def _get_python_binary():
    if env.python_version == 2:
        return PYTHON2_BIN
    elif env.python_version == 3:
        assert PYTHON3_BIN
    else:
        assert False

def _create_dir(name, owner, group = ''):
    if group == '':
        group = owner
    files.directory(name, use_sudo=True, owner=owner, group=group, mode='755')

class Deployer(object):
    def prepare_env_vars(self):
        """
        **`env` settings**
        env.user - deploy user name (used for ssh)
        env.password - deploy user password (used for ssh)
        env.hosts - list deploy hosts (used for ssh)

        env.domain - django site domain is used for:
            - nginx settings
            - uWSGI start user
            - project dir name

        env.repository - remote git repository url

        env.no_input - don't ask questings just use default values instead.
            Abort if env.domain (env.repository) value is not set or invalid.
        """
        global confirm
        require('no_input')
        if env.no_input:
            def confirm_local(question, default = True):
                puts(question)
                puts('Use no_input [default: {0}]'.format('Y' if default else 'N'))
                return default

            confirm = confirm_local
        else:
            confirm = confirm_global

        if not env.get('domain'):
            if env.no_input:
                abort('You need to set env.domain!')
            else:
                prompt('Project DNS url: ', 'domain', env.get('domain_default', ''), validate=VALIDATOR)
        else:
            if not re.findall(VALIDATOR, env.domain):
                abort('Invalid env.domain!')

        if not env.get('repository'):
            if env.no_input:
                env.repository = env.repository_default
            else:
                prompt('Deploy from: ', 'repository', env.get('repository_default', ''))

        require('repository', 'domain')
        puts('Deploy site: {0} \nFrom: {1}'.format(env.domain, env.repository))
        domain_without_dot   = env.domain.replace('.', '_')
        env.project_user     = domain_without_dot
        env.project_group    = domain_without_dot
        env.project_dir_name = domain_without_dot
        env.root = posixpath.join(PROJECTS_ROOT, env.project_dir_name)
        env.uwsgi_additional_param = ""

    def fetch_dependencies(self):
        nginx.server()
        uwsgi_plugin_python = "uwsgi-plugin-python"
        if env.python_version == 3:
            uwsgi_plugin_python += '3'

        deb.packages(['git', 'uwsgi', uwsgi_plugin_python])

        if hasattr(env, 'install_compass') and env.install_compass:
            deb.packages(['rubygems'])
            sudo('gem install compass --no-rdoc --no-ri')

        if hasattr(env, 'install_yuglify') and env.install_yuglify:
            from fabtools import nodejs
            #nodejs.installed_from_source()
            deb.packages(['npm'])
            sudo('ln -sf /usr/bin/nodejs /usr/bin/node')
            nodejs.install_package('yuglify')

    def set_up_db(self):
        from fabtools.require import postgres

        if hasattr(env, 'postgres_db') and env.postgres_db:
            ## HACK: fabtools.postgres uses run('sudo cmd') instead of sudo('cmd')
            ## to run psql commands and the password is asked every time
            ### Another and correct solution is to update fabtools from git repo:
            ### pip install git+git://github.com/ronnix/fabtools.git
            #def _run_as_pg(command):
            #    with cd('~postgres'):
            #        return sudo(command, user='postgres')
            #fabtools.postgres._run_as_pg = _run_as_pg

            deb.packages(['libpq-dev', 'python3-dev'])
            postgres.server()
            postgres.user(env.postgres_user, env.postgres_password)
            postgres.database(env.postgres_db, owner=env.postgres_user)

    def create_deploy_user(self):
        if not fabtools.user.exists('deploy'):
            fabtools.user.create('deploy', home=PROJECTS_ROOT, group='deploy', create_home=False, system=True, shell='/bin/false', create_group=True)


    def create_project_user(self):
        if not fabtools.user.exists(env.project_user):
            fabtools.user.create(env.project_user, home=env.root, group=env.project_group, create_home=False, system=True, shell='/bin/false', create_group=True)


    def get_latest_source(self):
        with cd(env.root):
            if not _is_dir('src') or not _is_dir('src/.git') or confirm('project src directory exists! [rm all and re clone / git pull]?', default=False):
                _create_dir('src', env.project_user)
                with cd('src'):
                    sudo('rm -Rf .??* *')
                    sudo('git clone %(repository)s .' % env, user=env.project_user)
            else:
                with cd('src'):
                    sudo('git pull', user=env.project_user)


    def update_settings(self):
        print('The method for updating settings is not implemented!')


    def update_virtual_env(self):
        pip_cache_dir = posixpath.join(PROJECTS_ROOT, '.pip.cache')
        _create_dir(pip_cache_dir, 'deploy')

        with cd(env.root):
            if not _is_dir('venv') or confirm('project venv directory exist! [rm all and recreate / repeat install]?', default=False):
                _create_dir('venv', 'deploy')
                with cd('venv'):
                    sudo('rm -Rf .??* *')
            python.virtualenv('venv', use_sudo=True, user='deploy', clear=True, venv_python=_get_python_binary())

            with fabtools.python.virtualenv('venv'):
                python.install_requirements('src/requirements.txt', use_sudo=True, user='deploy', download_cache=pip_cache_dir)


    def create_skeleton(self):
        _create_dir(env.root, 'root')
        with cd(env.root):
            _create_dir('log', 'root')
            _create_dir('db',     env.project_user, env.project_group)
            _create_dir('media',  env.project_user, env.project_group)
            _create_dir('static', env.project_user, env.project_group)
            sudo('chown -R ' + env.project_user + ':' + env.project_group + ' db* static* media*')

    def update_static_files(self):
        print("The method for updating static files is not implemented")

    def update_db(self):
        print("The method for updating database is not implemented")

    def set_up_http_server(self):
        env.uwsgi_plugin = "python"
        if env.python_version == 3:
            env.uwsgi_plugin += '3'

        with cd(env.root):
            if _is_dir('conf') and not confirm('project conf directory exists! [backup and update? / skip]', default=False):
                return
            _create_dir('conf', 'root')
            with cd('conf'):
                uwsgi_conf = os.path.join(LOCAL_CONF_TEMPLATES, 'uwsgi.ini')
                nginx_conf = os.path.join(LOCAL_CONF_TEMPLATES, 'nginx.conf')

                sudo('rm -Rf *.back')
                sudo('ls -d *{.conf,.ini} | sed \'s/.*$/mv -fu "&" "\\0.back"/\' | sh')
                files.template_file(
                    'uwsgi.ini',
                    template_source=uwsgi_conf, context=env, use_sudo=True,
                    owner='root', group='root', mode='644'
                )

                files.file('reload', use_sudo=True, owner='root', group='root')
                sudo('ln -sf $(pwd)/uwsgi.ini /etc/uwsgi/apps-enabled/' + env.project_dir_name + '.ini')

                files.template_file(
                    'nginx.conf',
                    template_source=nginx_conf, context=env, use_sudo=True,
                    owner='root', group='root', mode='644'
                )
                sudo('ln -sf $(pwd)/nginx.conf /etc/nginx/sites-enabled/' + env.project_dir_name)

    def restart_http_server(self):
        sudo('service nginx restart')
        sudo('service uwsgi restart')


class DjangoDeployer(Deployer):
    def prepare_env_vars(self):
        super(DjangoDeployer, self).prepare_env_vars()
        env.uwsgi_module = "django.core.handlers.wsgi:WSGIHandler()"
        env,uwsgi_additional_param = "env = DJANGO_SETTINGS_MODULE=project.settings"

    def update_settings(self):
        with cd(env.root):
            local_conf = os.path.join(LOCAL_CONF_TEMPLATES, 'local.txt')
            files.template_file('src/project/local.py', template_source=local_conf, context=env, use_sudo=True, owner=env.project_user, group=env.project_group, mode='755')

    def update_static_files(self):
        with cd(env.root):
            with fabtools.python.virtualenv('venv'):
                sudo('python src/manage.py collectstatic --noinput', user=env.project_user)


    def update_db(self):
        with cd(env.root):
            with fabtools.python.virtualenv('venv'):
                sudo('python src/manage.py syncdb  --noinput', user=env.project_user)
                sudo('python src/manage.py migrate --noinput', user=env.project_user)


class FlaskDeployer(Deployer):
    def prepare_env_vars(self):
        super(FlaskDeployer, self).prepare_env_vars()
        env.uwsgi_module = "project:app"


_deployer = None

def get_deployer():
    global _deployer
    if not _deployer:
        if env.project_type == "django":
            _deployer = DjangoDeployer()
        else:
            _deployer = FlaskDeployer()
    return _deployer

def set_deployer(deployer):
    global _deployer
    _deployer = deployer


def deploy():
    """
    Deploys a python web project from a git repository to a remote server.
    """
    d = get_deployer()
    d.prepare_env_vars()

    d.fetch_dependencies()
    d.create_deploy_user()
    d.set_up_db()

    _create_dir(PROJECTS_ROOT, 'root')
    if _is_dir(env.root) and confirm('project directory exists! abort?', default=False):
        return

    d.create_project_user()
    d.create_skeleton()

    d.get_latest_source()

    d.update_settings()
    d.update_virtual_env()
    d.update_static_files()
    d.update_db()

    d.set_up_http_server()
    d.restart_http_server()


def update_local_dependencies():
    """
    Updates local dependencies using 'pip install'
    """
    local('./venv/bin/pip install --download-cache=../deploy/.pip.cache -r ./src/requirements.txt'.replace('/', os.path.sep))

    if hasattr(env, 'install_compass') and env.install_compass:
        local('sudo apt-get install rubygems')
        local('sudo gem install compass --no-rdoc --no-ri')

    if hasattr(env, 'install_yuglify') and env.install_yuglify:
        local('sudo apt-get install npm')
        local('sudo ln -sf /usr/bin/nodejs /usr/bin/node')
        local('sudo npm install yuglify')


def destroy():
    """
    Removes a python web project site from a remote server.
    """
    d = get_deployer()
    d.prepare_env_vars()
    if not confirm_global('Are you sure you want to destroy the site?', default=False):
        return
    sudo('rm -Rf /etc/uwsgi/apps-enabled/' + env.project_dir_name + '.ini')
    sudo('rm -Rf /etc/nginx/sites-enabled/' + env.project_dir_name)
    sudo('rm -Rf ' + env.root)
    d.restart_http_server()


def _local_template_render(local_template, dict, local_target):
    local_file_template = os.path.join(os.path.dirname(__file__), 'template', local_template)
    local_out = os.path.join(env.lcwd, local_target)
    with open(local_file_template, 'r') as f:
        rendered = f.read().format(**dict)
        with open(local_out, 'w') as out:
            out.write(rendered)


def _start_project():
    template_path = os.path.join("..", "deploy", env.template)

    is_django = os.path.isfile(os.path.join(template_path, "manage.py"))
    if is_django:
        env.project_type = "django"
    else: # assume flask otherwise
        env.project_type = "flask"

    try:
        template_settings = import_file(os.path.join(template_path, 'fabsettings'))
        env.python_version = template_settings.PYTHON_VERSION
    except ImportError:
        env.python_version = PYTHON_VERSION

    with lcd(PROJECTS_ROOT):
        local('mkdir ' + env.project)
        with lcd(env.project):
            local('mkdir db venv static media')

            if is_django:
                local('django-admin.py startproject --template %s src' % (template_path,))
            else:
                local('mkdir src')
                local('cp -r %s/* src/' % (template_path,))
                local('rm src/fabsettings.*')

            with lcd('src'):
                _local_template_render('gitignore.txt', env, '.gitignore')
                local('git init')
                local('git add *')
                local('git commit -am "init"')

            local('virtualenv --clear --python=%s venv' % (_get_python_binary(),))
            update_local_dependencies()

            if is_django:
                local('./venv/bin/python ./src/manage.py syncdb --noinput && ./venv/bin/python ./src/manage.py migrate --noinput'.replace('/', os.path.sep))


def _create_bitbucket_repo_impl():
    with lcd(PROJECTS_ROOT):
        env.bit_user = BITBUCKET_USER
        env.bit_password = BITBUCKET_PASSWORD

        import requests as r
        rez = r.post('https://api.bitbucket.org/1.0/repositories/',
            data=dict(name=env.project, is_private=True),
            auth=(BITBUCKET_USER, BITBUCKET_PASSWORD)
        )
        puts('request status ok: {0}'.format(rez.ok))

        if rez.ok:
            with lcd(env.project):
                _local_template_render('fabfile.txt', env, 'fabfile.py')
                with lcd('src'):
                    local('git remote add origin git@bitbucket.org:{0}/{1}.git'.format(BITBUCKET_USER, env.project))
                    local('git push -u origin --all')


def init():
    """
    Creates new django project and maybe uploads it to the bitbucket.
    """
    prompt('project domain: ', 'project', validate=VALIDATOR)
    prompt('project template: ', 'template', default='project', validate=VALIDATOR)

    puts('create project: {0}'.format(env.project))
    puts('using template: {0}'.format(env.template))

    _start_project()
    if BITBUCKET_USER and BITBUCKET_PASSWORD and confirm_global('create private bitbucket repository?'):
        _create_bitbucket_repo_impl()


def create_bitbucket_repo():
    """
    Uploads the project to the bitbucket.
    """
    if not BITBUCKET_USER or not BITBUCKET_PASSWORD:
        abort('You need to set BITBUCKET_USER and BITBUCKET_PASSWORD in your fabsettings.py')
    prompt('project domain: ', 'project', validate=VALIDATOR)
    puts('create git repository on bitbucket: {0}'.format(env.project))
    _create_bitbucket_repo_impl()


if __name__ == '__main__':
    import subprocess
    import sys
    subprocess.call(['fab', '-f', __file__] + sys.argv[1:])
